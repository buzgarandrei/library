package com.example.library.utils;

import com.example.library.model.Book;
import lombok.extern.log4j.Log4j2;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;

@Log4j2
public class Utils {

    public static LocalDateTime calculateEndDateForLoaningBook(Book book) {
        LocalDateTime initial = LocalDateTime.now();
        LocalDateTime endDate = initial.plusDays(book.getDaysForLoaning());
        log.info("end date is: " + endDate);
        return endDate;
    }

    public static BigDecimal calculatePenalty(BigDecimal dailyFee, LocalDateTime endDate) {
        LocalDateTime currentTime = LocalDateTime.now();
        LocalDateTime difference = LocalDateTime.from(endDate);
        long days = difference.until(currentTime, ChronoUnit.DAYS);
        log.info("number of delay days is: " +days);
        log.info("daily fee for this book: " + dailyFee.longValue());
        log.info("penalty in long is: " +BigDecimal.valueOf(days).multiply(dailyFee));
        return BigDecimal.valueOf(days).multiply(dailyFee);
    }
}
