package com.example.library.mapper;

import com.example.library.model.Appointment;
import com.example.library.model.dto.AppointmentDTO;
import org.springframework.stereotype.Component;

@Component
public class AppointmentMapper<Ap, A> implements Mapper<Appointment, AppointmentDTO> {

    @Override
    public AppointmentDTO convertToDto(Appointment appointment) {

        AppointmentDTO appointmentDTO = new AppointmentDTO();
        appointmentDTO.setBookName(appointment.getBook().getName());
        appointmentDTO.setUserName(appointment.getUser().getFullName());
        appointmentDTO.setStartDate(appointment.getStartDate());
        appointmentDTO.setEndDate(appointment.getEndDate());
        appointmentDTO.setReturned(appointment.getReturned());
        appointmentDTO.setPenaltyAmount(appointment.getPenaltyAmount());
        appointmentDTO.setBookId(appointment.getBook().getId());
        appointmentDTO.setUserId(appointment.getUser().getId());
        appointmentDTO.setId(appointment.getId());

        return appointmentDTO;
    }

    @Override
    public Appointment convertToModel(AppointmentDTO appointmentDTO) {

        Appointment appointment = new Appointment();
        appointment.setStartDate(appointmentDTO.getStartDate());
        appointment.setReturned(appointmentDTO.getReturned());
        appointment.setPenaltyAmount(appointmentDTO.getPenaltyAmount());

        return appointment;
    }
}
