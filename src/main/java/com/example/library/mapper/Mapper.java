package com.example.library.mapper;

public interface Mapper <M, D> {

    D convertToDto(M m);

    M convertToModel(D d);
}
