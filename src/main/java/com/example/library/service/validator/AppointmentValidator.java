package com.example.library.service.validator;

import com.example.library.exception.AppointmentNotFoundException;
import com.example.library.exception.BookNotFoundException;
import com.example.library.exception.UserNotFoundException;
import com.example.library.model.dto.AppointmentDTO;

public interface AppointmentValidator {

    void validateBook(Long id) throws BookNotFoundException;

    void validateUser(Long id) throws UserNotFoundException;

    void validate(AppointmentDTO appointmentDTO) throws BookNotFoundException, UserNotFoundException;

    void validateReturnBookRequest(AppointmentDTO appointmentDTO) throws AppointmentNotFoundException, BookNotFoundException, UserNotFoundException;
}
