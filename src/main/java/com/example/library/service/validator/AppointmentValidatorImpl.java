package com.example.library.service.validator;

import com.example.library.exception.AppointmentNotFoundException;
import com.example.library.exception.BookNotFoundException;
import com.example.library.exception.UserNotFoundException;
import com.example.library.model.Appointment;
import com.example.library.model.Book;
import com.example.library.model.User;
import com.example.library.model.dto.AppointmentDTO;
import com.example.library.repository.AppointmentRepository;
import com.example.library.repository.BookRepository;
import com.example.library.repository.UserRepository;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

@Service
@Log4j2
@Transactional
public class AppointmentValidatorImpl implements AppointmentValidator {

    private final BookRepository bookRepository;
    private final UserRepository userRepository;
    private final AppointmentRepository appointmentRepository;

    @Autowired
    public AppointmentValidatorImpl(BookRepository bookRepository, UserRepository userRepository,
                                    AppointmentRepository appointmentRepository) {

        this.bookRepository = bookRepository;
        this.userRepository = userRepository;
        this.appointmentRepository = appointmentRepository;
    }

    @Override
    public void validateBook(Long id) throws BookNotFoundException {

        Optional<Book> book = bookRepository.findById(id);
        if (book.isEmpty()) {
            throw new BookNotFoundException("Book not found for given id");
        }
    }

    @Override
    public void validateUser(Long id) throws UserNotFoundException {

        Optional<User> user = userRepository.findById(id);
        if (user.isEmpty()) {
            throw new UserNotFoundException("User not found for the given id");
        }
    }

    @Override
    public void validate(AppointmentDTO appointmentDTO) throws BookNotFoundException, UserNotFoundException {

        validateBook(appointmentDTO.getBookId());
        validateUser(appointmentDTO.getUserId());
    }

    @Override
    public void validateReturnBookRequest(AppointmentDTO appointmentDTO) throws AppointmentNotFoundException, BookNotFoundException, UserNotFoundException {

        validateAppointment(appointmentDTO.getId());
        validateUserMadeInitialAppointment(appointmentDTO);
        validateBookBeingReturnedToTheInitialAppointment(appointmentDTO);
    }

    private void validateUserMadeInitialAppointment(AppointmentDTO appointmentDTO) throws UserNotFoundException {

        Optional<User> user = userRepository.findById(appointmentDTO.getUserId());
        Optional<Appointment> appointment = appointmentRepository.findById(appointmentDTO.getId());
        Optional<User> userFromAppointment = userRepository.findById(appointment.orElse(null).getUser().getId());

        log.info("user :" + user);
        log.info("appointment :" + appointment);
        log.info(" user from appointment" + userFromAppointment);
        if (user.isEmpty()) {
            throw new UserNotFoundException("No user found for given user");
        }
        if (!user.equals(userFromAppointment)) {
            throw new UserNotFoundException("The given user does not correspond to the user that made the initial appointment");
        }

    }

    private void validateBookBeingReturnedToTheInitialAppointment(AppointmentDTO appointmentDTO) throws BookNotFoundException {

        Optional<Book> book = bookRepository.findById(appointmentDTO.getBookId());
        Optional<Appointment> appointment = appointmentRepository.findById(appointmentDTO.getId());
        Optional<Book> bookFromAppointment = bookRepository.findById(appointment.orElse(null).getId());

        if (book.isEmpty()) {
            throw new BookNotFoundException("No book found for given id");
        }
        if (!book.equals(bookFromAppointment)) {
            throw new BookNotFoundException("The given book does not correspond to the book from the initial appointment");
        }
    }

    private void validateAppointment(Long id) throws AppointmentNotFoundException {

        Optional<Appointment> appointment = appointmentRepository.findById(id);
        if (appointment.isEmpty()) {
            throw new AppointmentNotFoundException("Trying to update not existing appointment");
        }
    }
}
