package com.example.library.service;

public interface BootstrapService {
    void populateDB();
}
