package com.example.library.service;

import com.example.library.model.Book;
import com.example.library.model.RoleEnum;
import com.example.library.model.User;
import com.example.library.model.UserProfile;
import com.example.library.repository.BookRepository;
import com.example.library.repository.UserProfileRepository;
import com.example.library.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.Optional;

@Service
@Transactional
public class BootstrapServiceImpl implements BootstrapService {

    private final UserRepository userRepository;
    private final UserProfileRepository userProfileRepository;
    private final BookRepository bookRepository;

    @Autowired
    public BootstrapServiceImpl(UserRepository userRepository, UserProfileRepository userProfileRepository,
                                BookRepository bookRepository) {
        this.userRepository = userRepository;
        this.userProfileRepository = userProfileRepository;
        this.bookRepository = bookRepository;
    }

    @Override
    public void populateDB() {
        populateUser();
        populateBooks();
    }

    void populateUser() {

        UserProfile andreiProfile = new UserProfile();
        andreiProfile.setAddress("Cluj-Napoca, Random Street 12, ap.30");
        andreiProfile.setPersonalIdentityCode("198903682947");

        User andrei = new User();
        andrei.setEmail("andrei.buzgar@nttdata.ro");
        andrei.setFullName("Buzgar Andrei");
        andrei.setRole(RoleEnum.CLIENT);
        andrei.setPassword("111");
        userRepository.save(andrei);

        andreiProfile.setUser(andrei);
        userProfileRepository.save(andreiProfile);
    }

    void populateBooks() {

        Optional<User> andrei = userRepository.findById(1L);
        Book idiotByDostoevsky = new Book();
        idiotByDostoevsky.setAuthor("Dostoevsky");
        idiotByDostoevsky.setEdition(2);
        idiotByDostoevsky.setIsAvailable(true);
        idiotByDostoevsky.setPublishingHouse("RAO Publishing House");
        idiotByDostoevsky.setDaysForLoaning(8);
        idiotByDostoevsky.setName("the Idiot");
        idiotByDostoevsky.setDailyFeeForDelay(BigDecimal.valueOf(0.7));
        idiotByDostoevsky.getClientList().add(andrei.get());
        andrei.get().getBooks().add(idiotByDostoevsky);

        bookRepository.save(idiotByDostoevsky);
    }

}
