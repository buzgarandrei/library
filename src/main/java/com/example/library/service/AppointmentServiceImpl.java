package com.example.library.service;

import com.example.library.exception.AppointmentNotFoundException;
import com.example.library.exception.BookNotFoundException;
import com.example.library.exception.UserNotFoundException;
import com.example.library.mapper.AppointmentMapper;
import com.example.library.model.Appointment;
import com.example.library.model.Book;
import com.example.library.model.User;
import com.example.library.model.dto.AppointmentDTO;
import com.example.library.repository.AppointmentRepository;
import com.example.library.repository.BookRepository;
import com.example.library.repository.UserRepository;
import com.example.library.service.validator.AppointmentValidator;
import com.example.library.utils.Utils;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.time.LocalDateTime;

@Service
@Log4j2
@Transactional
public class AppointmentServiceImpl implements AppointmentService {

    private AppointmentRepository appointmentRepository;
    private AppointmentMapper<Appointment, AppointmentDTO> appointmentMapper;
    private AppointmentValidator appointmentValidator;

    private BookRepository bookRepository;
    private UserRepository userRepository;

    @Autowired
    public AppointmentServiceImpl(AppointmentRepository appointmentRepository, AppointmentValidator appointmentValidator,
                                  AppointmentMapper<Appointment,AppointmentDTO> appointmentMapper,
                                  BookRepository bookRepository, UserRepository userRepository ) {
        this.appointmentRepository = appointmentRepository;
        this.appointmentValidator = appointmentValidator;
        this.appointmentMapper= appointmentMapper;
        this.bookRepository = bookRepository;
        this.userRepository = userRepository;
    }

    @Override
    public AppointmentDTO makeAppointment(AppointmentDTO appointmentDTO) throws UserNotFoundException, BookNotFoundException {

        appointmentValidator.validate(appointmentDTO);
        Book book = bookRepository.findById(appointmentDTO.getBookId()).orElse(null);
        User user = userRepository.findById(appointmentDTO.getUserId()).orElse(null);
        appointmentDTO.setStartDate(LocalDateTime.now());
        Appointment appointment = appointmentMapper.convertToModel(appointmentDTO);
        appointment.setPenaltyAmount(BigDecimal.valueOf(0));
        appointment.setReturned(Boolean.FALSE);
        appointment.setBook(book);
        appointment.setUser(user);

        appointment.setEndDate(Utils.calculateEndDateForLoaningBook(book));
        appointmentRepository.save(appointment);

        AppointmentDTO appointmentResponse = appointmentMapper.convertToDto(appointment);
        log.info("the appointment response dto is:  " + appointmentDTO);

        return appointmentResponse;
    }

    @Override
    public AppointmentDTO returnBook(AppointmentDTO appointmentDTO)
            throws UserNotFoundException, BookNotFoundException, AppointmentNotFoundException {

        appointmentValidator.validateReturnBookRequest(appointmentDTO);


        Appointment appointment = appointmentRepository.findById(appointmentDTO.getId()).orElse(null);
        appointment.setReturned(Boolean.TRUE);

        BigDecimal decimalAmount = Utils.calculatePenalty(appointment.getBook().getDailyFeeForDelay(), appointment.getEndDate());
        log.info("amount to be payed is: " + decimalAmount);
        log.info("amount in long is: " + decimalAmount.longValue());
        if (decimalAmount.longValue() > 0) {
            appointment.setPenaltyAmount(decimalAmount);
        }
        else {
            appointment.setPenaltyAmount(BigDecimal.valueOf(0));
        }
        appointmentRepository.save(appointment);

        AppointmentDTO appointmentResponse = appointmentMapper.convertToDto(appointment);
        return appointmentResponse;

    }

    @Override
    public boolean delete(Long id) {
        appointmentRepository.deleteById(id);
        return true;
    }

}
