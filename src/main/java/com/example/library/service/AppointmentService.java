package com.example.library.service;

import com.example.library.exception.AppointmentNotFoundException;
import com.example.library.exception.BookNotFoundException;
import com.example.library.exception.UserNotFoundException;
import com.example.library.model.dto.AppointmentDTO;

public interface AppointmentService {
    AppointmentDTO makeAppointment(AppointmentDTO appointmentDTO) throws UserNotFoundException, BookNotFoundException;

    public boolean delete(Long id);

    AppointmentDTO returnBook(AppointmentDTO appointmentDTO) throws UserNotFoundException, BookNotFoundException, AppointmentNotFoundException;
}
