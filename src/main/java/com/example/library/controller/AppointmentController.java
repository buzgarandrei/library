package com.example.library.controller;

import com.example.library.exception.BookNotFoundException;
import com.example.library.exception.UserNotFoundException;
import com.example.library.model.dto.AppointmentDTO;
import com.example.library.service.AppointmentService;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

@RestController
@Log4j2
public class AppointmentController {

    private AppointmentService appointmentService;

    @Autowired
    public AppointmentController(AppointmentService appointmentService) {
        this.appointmentService = appointmentService;
    }

    @PostMapping("makeAppointment")
    public ResponseEntity<AppointmentDTO> makeAppointment(@RequestBody AppointmentDTO appointmentDTO) {
        try {
            return ResponseEntity.status(HttpStatus.CREATED).body(appointmentService.makeAppointment(appointmentDTO));
        }
        catch (UserNotFoundException | BookNotFoundException e) {
            log.error(e);
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage());
        }
    }
    @PutMapping("returnBook")
    public ResponseEntity<AppointmentDTO> returnBook(@RequestBody AppointmentDTO appointmentDTO) {
        try {
            return ResponseEntity.status(HttpStatus.ACCEPTED).body(appointmentService.returnBook(appointmentDTO));
        }
        catch (Exception e) {
            log.error(e);
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage());
        }
    }

    @DeleteMapping("deleteAppointment/{id}")
    public ResponseEntity<Boolean> deleteAppointment(@PathVariable Long id) {
        try{
            return ResponseEntity.status(HttpStatus.NO_CONTENT).body(appointmentService.delete(id));
        }
        catch (Exception e) {
            e.getStackTrace();
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST,e.getMessage());
        }
    }
}
