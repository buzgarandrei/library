package com.example.library.controller;

import com.example.library.exception.BookNotFoundException;
import com.example.library.exception.ErrorMessage;
import com.example.library.exception.UserNotFoundException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

@RestControllerAdvice
public class LibraryExceptionHandler {

    @ExceptionHandler(Exception.class)
    public ResponseEntity<String> teResponse(Exception e) {

        ErrorMessage errorMessage = new ErrorMessage(e.getMessage(), 400);
        return new ResponseEntity<>(e.getMessage(), HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(BookNotFoundException.class)
    public ResponseEntity<String> toResponse(BookNotFoundException e) {

        ErrorMessage errorMessage = new ErrorMessage(e.getMessage(), 400);
        return new ResponseEntity<>(e.getMessage(), HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(UserNotFoundException.class)
    public ResponseEntity<String> toResponse(UserNotFoundException e) {

        ErrorMessage errorMessage = new ErrorMessage(e.getMessage(), 400);
        return new ResponseEntity<>(e.getMessage(), HttpStatus.BAD_REQUEST);
    }
}
