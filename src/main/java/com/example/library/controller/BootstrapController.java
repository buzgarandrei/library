package com.example.library.controller;

import com.example.library.service.BootstrapService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class BootstrapController {

    private BootstrapService bootstrapService;

    @Autowired
    public BootstrapController(BootstrapService bootstrapService) {
        this.bootstrapService = bootstrapService;
    }

    @GetMapping("populate")
    public void populateDB() {
        bootstrapService.populateDB();
    }
}
