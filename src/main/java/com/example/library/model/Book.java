package com.example.library.model;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

@Entity
@Data
@EqualsAndHashCode(exclude = "clientList")
@ToString(exclude = "clientList")
public class Book {

    @Id @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String name;

    private String author;

    private String publishingHouse;

    private Integer edition;

    private Boolean isAvailable;

    private Integer daysForLoaning;

    private BigDecimal dailyFeeForDelay;

    @ManyToMany(mappedBy = "books", cascade = CascadeType.ALL)
    private List<User> clientList = new ArrayList<>();
}
