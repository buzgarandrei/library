package com.example.library.model.dto;


import lombok.Data;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;

@Data
public class AppointmentDTO {

    private Long id;
    private LocalDateTime startDate;
    private LocalDateTime endDate;
    private String userName;
    private Long userId;
    private String bookName;
    private Long bookId;
    private Boolean returned;
    private BigDecimal penaltyAmount;
}
