package com.example.library.mapper;

import com.example.library.model.Appointment;
import com.example.library.model.Book;
import com.example.library.model.User;
import com.example.library.model.dto.AppointmentDTO;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.math.BigDecimal;
import java.time.LocalDateTime;

import static org.junit.jupiter.api.Assertions.*;

class AppointmentMapperTest {

    private AppointmentMapper<Appointment, AppointmentDTO> appointmentMapper;

    private Appointment appointment;

    private AppointmentDTO appointmentDTO;

    @BeforeEach
    void setUp() {
        appointmentMapper = new AppointmentMapper<>();
        User user = new User();
        user.setId(1L);
        user.setFullName("Andrei Buzgar");
        user.setEmail("andrei.buzgar@nttdata.com");

        Book book = new Book();
        book.setId(1L);
        book.setDailyFeeForDelay(BigDecimal.valueOf(1.3));
        book.setName("The three musketeers");

        appointment = new Appointment();
        appointment.setPenaltyAmount(BigDecimal.valueOf(0.0));
        appointment.setReturned(Boolean.FALSE);
        appointment.setStartDate(LocalDateTime.now());
        appointment.setEndDate(LocalDateTime.now().plusDays(7));
        appointment.setUser(user);
        appointment.setBook(book);

        appointmentDTO = new AppointmentDTO();
        appointmentDTO.setStartDate(LocalDateTime.now());
        appointmentDTO.setEndDate(LocalDateTime.now().plusDays(7));
        appointmentDTO.setPenaltyAmount(BigDecimal.valueOf(0.0));
        appointmentDTO.setReturned(Boolean.FALSE);
        appointmentDTO.setUserId(1L);
        appointmentDTO.setBookId(1L);
        appointmentDTO.setBookName("The three musketeers");
        appointmentDTO.setUserName("Andrei Buzgar");
    }

    @Test
    void convertToDto() {
        assertEquals(appointmentDTO, appointmentMapper.convertToDto(appointment));
    }

    @Test
    void convertToModel() {
        appointment.setEndDate(null);
        appointment.setUser(null);
        appointment.setBook(null);
        assertEquals(appointment, appointmentMapper.convertToModel(appointmentDTO));
    }
}