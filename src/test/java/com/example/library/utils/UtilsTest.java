package com.example.library.utils;

import com.example.library.model.Book;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.mockito.junit.jupiter.MockitoSettings;
import org.mockito.quality.Strictness;
import org.springframework.test.context.junit4.SpringRunner;

import java.math.BigDecimal;
import java.time.LocalDateTime;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
@MockitoSettings(strictness = Strictness.LENIENT)
class UtilsTest {

    @Mock
    private Book book;

    @BeforeEach
    void setUp() {
        when(book.getDaysForLoaning()).thenReturn(4);
    }

    @Test
    void calculateEndDateForLoaningBook() {
        assertEquals(LocalDateTime.now().plusDays(4), Utils.calculateEndDateForLoaningBook(book));
    }

    @Test
    void calculatePenalty() {
        assertEquals(BigDecimal.valueOf(1.4), Utils.calculatePenalty(BigDecimal.valueOf(0.7), LocalDateTime.now().minusDays(2)));
    }
}