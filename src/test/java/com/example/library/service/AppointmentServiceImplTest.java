package com.example.library.service;

import com.example.library.exception.AppointmentNotFoundException;
import com.example.library.exception.BookNotFoundException;
import com.example.library.exception.UserNotFoundException;
import com.example.library.mapper.AppointmentMapper;
import com.example.library.model.Appointment;
import com.example.library.model.Book;
import com.example.library.model.User;
import com.example.library.model.dto.AppointmentDTO;
import com.example.library.repository.AppointmentRepository;
import com.example.library.repository.BookRepository;
import com.example.library.repository.UserRepository;
import com.example.library.service.validator.AppointmentValidator;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.ArgumentMatchers.nullable;
import static org.mockito.Mockito.when;

@SpringBootTest
class AppointmentServiceImplTest {


    private AppointmentService appointmentService;

    @Mock
    private AppointmentRepository appointmentRepository;

    @Autowired
    private AppointmentValidator appointmentValidator;

    @Mock
    private AppointmentMapper<Appointment, AppointmentDTO> appointmentMapper;

    @Mock
    private BookRepository bookRepository;

    @Mock
    private UserRepository userRepository;

    private Appointment appointment;
    private AppointmentDTO appointmentDTO;
    private Book book;
    private User user;

    @BeforeEach
    void setUp() {

        appointmentService = new AppointmentServiceImpl(appointmentRepository, appointmentValidator,
                appointmentMapper, bookRepository, userRepository);

        user = new User();
        user.setId(1L);
        user.setFullName("Andrei Buzgar");
        user.setEmail("andrei.buzgar@nttdata.com");

        book = new Book();
        book.setId(1L);
        book.setDailyFeeForDelay(BigDecimal.valueOf(1.3));
        book.setDaysForLoaning(8);
        book.setName("The three musketeers");

        appointment = new Appointment();
        appointment.setPenaltyAmount(BigDecimal.valueOf(0.0));
        appointment.setReturned(Boolean.FALSE);
        appointment.setStartDate(LocalDateTime.now());
        appointment.setEndDate(LocalDateTime.now().plusDays(7));
        appointment.setUser(user);
        appointment.setBook(book);

        appointmentDTO = new AppointmentDTO();
        appointmentDTO.setStartDate(LocalDateTime.now());
        appointmentDTO.setEndDate(LocalDateTime.now().plusDays(7));
        appointmentDTO.setPenaltyAmount(BigDecimal.valueOf(0.0));
        appointmentDTO.setReturned(Boolean.FALSE);
        appointmentDTO.setUserId(1L);
        appointmentDTO.setBookId(1L);
        appointmentDTO.setBookName("The three musketeers");
        appointmentDTO.setUserName("Andrei Buzgar");
        appointmentDTO.setId(1L);

        when(appointmentMapper.convertToModel(appointmentDTO)).thenReturn(appointment);
        when(appointmentRepository.findById(anyLong())).thenReturn(Optional.ofNullable(appointment));

        when(bookRepository.findById(appointmentDTO.getBookId())).thenReturn(Optional.ofNullable(book));
        when(userRepository.findById(appointmentDTO.getUserId())).thenReturn(Optional.ofNullable(user));

        when(appointmentMapper.convertToDto(appointment)).thenReturn(appointmentDTO);
    }

    @Test
    void makeAppointment() throws UserNotFoundException, BookNotFoundException {

        assertDoesNotThrow(() -> appointmentService.makeAppointment(appointmentDTO));
        assertEquals(appointmentDTO, appointmentService.makeAppointment(appointmentDTO));
    }

    @Test
    void returnBook() throws UserNotFoundException, BookNotFoundException, AppointmentNotFoundException {

        assertDoesNotThrow(()->appointmentService.returnBook(appointmentDTO));
        assertEquals(BigDecimal.valueOf(0.0), appointmentService.returnBook(appointmentDTO).getPenaltyAmount());
    }

    @Test
    void delete() {
    }
}